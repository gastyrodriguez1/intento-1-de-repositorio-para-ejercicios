package objetosEnClase.modelo;

public interface Model {
	public boolean equals(Object obj);
	public int hashCode();
	public String toString();
	
}
