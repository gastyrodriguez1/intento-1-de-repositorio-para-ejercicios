package objetosEnClase.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import objetosEnClase.controller.FiguraController;
import objetosEnClase.modelo.Circulo;
import objetosEnClase.modelo.Cuadrado;
import objetosEnClase.modelo.Figura;
import objetosEnClase.modelo.Rectangulo;
import objetosEnClase.modelo.TrianguloRectangulo;
import objetosEnClase.modelo.exception.FiguraException;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import javax.swing.SwingConstants;

public class FiguraView {

	private JFrame frame;
	private JTextField textValor;
	private JTable tablFiguras;
	
	private List<Figura> figuras;
	private String arrayfiguras[][];
	private Figura figuraAmodificarEliminar;
	private JTextField textNombre;
	private JTextField textBase;
	private JTextField textField_2;
	private JTextField textAltura;
	private JTextField textField_4;
	private JTextField textField_5;
	private JLabel lblSuperficieMaxima;
	private FiguraController figuraController = new FiguraController();


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FiguraView window = new FiguraView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public FiguraView() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.ORANGE);
		frame.setBounds(10, 10, 724, 697);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Rodr\u00EDguez Gast\u00F3n");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Arial", Font.ITALIC, 27));
		lblNewLabel.setBounds(115, 29, 262, 30);
		frame.getContentPane().add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBorder(new TitledBorder(null, "Una variable", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(263, 136, 422, 95);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblValor = new JLabel("Lado/Radio");
		lblValor.setBounds(97, 30, 99, 20);
		panel.add(lblValor);
		lblValor.setFont(new Font("Calibri", Font.BOLD, 16));
		
		textValor = new JTextField();
		textValor.setBounds(240, 30, 112, 20);
		panel.add(textValor);
		textValor.setFont(new Font("Tahoma", Font.BOLD, 16));
		textValor.setColumns(10);
		
		JButton btnCrearCuadrado = new JButton("Crear cuadrado");
		btnCrearCuadrado.setBounds(20, 57, 176, 29);
		panel.add(btnCrearCuadrado);
		btnCrearCuadrado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cuadrado cua =new Cuadrado(textNombre.getText(), Float.parseFloat(textValor.getText()));
				try {
					figuraController.addHandler(cua);
					figuras.add(cua);
					
					llenarGrilla(figuras);
					limpiarCampos();
				} catch (FiguraException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}
	
					}
		});
		btnCrearCuadrado.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		
		JButton btnCrearCirculo = new JButton("Crear circulo");
		btnCrearCirculo.setBounds(223, 57, 176, 29);
		panel.add(btnCrearCirculo);
		btnCrearCirculo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Circulo cir = 	new Circulo(textNombre.getText(), Float.parseFloat(textValor.getText()));
				try {
					figuraController.addHandler(cir);
					figuras.add(cir);
				} catch (FiguraException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}
				
		
				llenarGrilla(figuras);
				limpiarCampos();
			}
		});
		btnCrearCirculo.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBackground(Color.BLUE);
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(figuraAmodificarEliminar instanceof Cuadrado){
					Cuadrado cuad = (Cuadrado) figuraAmodificarEliminar;
					cuad.setNombre(textNombre.getText());
					cuad.setLado(Float.parseFloat(textValor.getText()));
				}
				else if(figuraAmodificarEliminar instanceof Circulo){
					Circulo cir = (Circulo) figuraAmodificarEliminar;
					cir.setNombre(textNombre.getText());
					cir.setRadio(Float.parseFloat(textValor.getText()));
				}
				else if(figuraAmodificarEliminar instanceof Rectangulo){
					Rectangulo rec = (Rectangulo) figuraAmodificarEliminar;
					rec.setBase(Float.parseFloat(textBase.getText()));
					rec.setAltura(Float.parseFloat(textAltura.getText()));
				}
					
				
				llenarGrilla(figuras);
				figuraAmodificarEliminar = null;
				limpiarCampos();
			}
		});
		btnModificar.setFont(new Font("Ubuntu", Font.ITALIC, 16));
		btnModificar.setBounds(26, 201, 176, 30);
		frame.getContentPane().add(btnModificar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setBackground(Color.GREEN);
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				figuras.remove(figuraAmodificarEliminar);
				llenarGrilla(figuras);
				limpiarCampos();
				figuraAmodificarEliminar=null;
			}
		});
		btnEliminar.setFont(new Font("Ubuntu", Font.ITALIC, 16));
		btnEliminar.setBounds(26, 274, 176, 31);
		frame.getContentPane().add(btnEliminar);
		
		JButton btnLimpiarCampos = new JButton("Limpiar campos");
		btnLimpiarCampos.setBackground(Color.YELLOW);
		btnLimpiarCampos.setForeground(Color.BLACK);
		btnLimpiarCampos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		btnLimpiarCampos.setFont(new Font("Ubuntu", Font.ITALIC, 16));
		btnLimpiarCampos.setBounds(26, 350, 176, 32);
		frame.getContentPane().add(btnLimpiarCampos);
		
		JLabel lblNombre = new JLabel("Nombre de la figura");
		lblNombre.setFont(new Font("Ubuntu", Font.BOLD, 17));
		lblNombre.setBounds(398, 60, 162, 20);
		frame.getContentPane().add(lblNombre);
		
		textNombre = new JTextField();
		textNombre.setFont(new Font("Tahoma", Font.BOLD, 16));
		textNombre.setColumns(10);
		textNombre.setBounds(404, 91, 141, 20);
		frame.getContentPane().add(textNombre);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.MAGENTA);
		panel_1.setLayout(null);
		panel_1.setBorder(new TitledBorder(null, "Dos variables", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(263, 247, 410, 105);
		frame.getContentPane().add(panel_1);
		
		JLabel lblBase = new JLabel("Base");
		lblBase.setFont(new Font("Calibri", Font.BOLD, 20));
		lblBase.setBounds(10, 33, 60, 20);
		panel_1.add(lblBase);
		
		textBase = new JTextField();
		textBase.setFont(new Font("Tahoma", Font.BOLD, 16));
		textBase.setColumns(10);
		textBase.setBounds(80, 30, 112, 20);
		panel_1.add(textBase);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setFont(new Font("Calibri", Font.BOLD, 20));
		lblAltura.setBounds(10, 74, 60, 20);
		panel_1.add(lblAltura);
		
		textAltura = new JTextField();
		textAltura.setFont(new Font("Tahoma", Font.BOLD, 16));
		textAltura.setColumns(10);
		textAltura.setBounds(80, 74, 112, 20);
		panel_1.add(textAltura);
		
		JButton btnCrearRectangulo = new JButton("Crear Rect\u00E1ngulo");
		btnCrearRectangulo.setBounds(220, 26, 176, 29);
		panel_1.add(btnCrearRectangulo);
		btnCrearRectangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				figuras.add(new Rectangulo(	textNombre.getText(), 
											Float.parseFloat(textBase.getText()), 
											Float.parseFloat(textAltura.getText())));
				llenarGrilla(figuras);
				limpiarCampos();
				
			}
		});
		btnCrearRectangulo.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		
		JButton btnCrearTriangulo = new JButton("Crear Tri\u00E1ngulo");
		btnCrearTriangulo.setBounds(220, 70, 176, 29);
		panel_1.add(btnCrearTriangulo);
		btnCrearTriangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				figuras.add(new TrianguloRectangulo(textNombre.getText(),
							Float.parseFloat(textBase.getText()), 
							Float.parseFloat(textAltura.getText())));
				llenarGrilla(figuras);
				limpiarCampos();

				
			}
		});
		btnCrearTriangulo.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.PINK);
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(null, "Tres variables", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(225, 361, 460, 122);
		frame.getContentPane().add(panel_2);
		
		JLabel lblValor_2 = new JLabel("Lado");
		lblValor_2.setFont(new Font("Calibri", Font.BOLD, 20));
		lblValor_2.setBounds(10, 30, 54, 20);
		panel_2.add(lblValor_2);
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		textField_2.setColumns(10);
		textField_2.setBounds(74, 30, 112, 20);
		panel_2.add(textField_2);
		
		JButton btnCrearCuadrado_2 = new JButton("Crear pol\u00EDgono");
		btnCrearCuadrado_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnCrearCuadrado_2.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		btnCrearCuadrado_2.setBounds(276, 21, 176, 29);
		panel_2.add(btnCrearCuadrado_2);
		
		JLabel lblValor_2_1 = new JLabel("Cantidad de lados");
		lblValor_2_1.setFont(new Font("Calibri", Font.BOLD, 20));
		lblValor_2_1.setBounds(10, 61, 176, 20);
		panel_2.add(lblValor_2_1);
		
		textField_4 = new JTextField();
		textField_4.setFont(new Font("Tahoma", Font.BOLD, 16));
		textField_4.setColumns(10);
		textField_4.setBounds(164, 61, 112, 20);
		panel_2.add(textField_4);
		
		JLabel lblValor_2_2 = new JLabel("Apotema");
		lblValor_2_2.setFont(new Font("Calibri", Font.BOLD, 20));
		lblValor_2_2.setBounds(10, 92, 97, 20);
		panel_2.add(lblValor_2_2);
		
		textField_5 = new JTextField();
		textField_5.setFont(new Font("Tahoma", Font.BOLD, 16));
		textField_5.setColumns(10);
		textField_5.setBounds(102, 92, 112, 20);
		panel_2.add(textField_5);
				
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(10, 513, 675, 134);
				frame.getContentPane().add(scrollPane);
				
				tablFiguras = new JTable();
				tablFiguras.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						
						figuraAmodificarEliminar= figuras.get(tablFiguras.getSelectedRow());
						
						if(figuraAmodificarEliminar instanceof Cuadrado)
							asignarValores((Cuadrado)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof Circulo)
							asignarValores((Circulo)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof Rectangulo)
							asignarValores((Rectangulo)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof TrianguloRectangulo)	
							asignarValores((TrianguloRectangulo)figuraAmodificarEliminar);
					}
				});
				tablFiguras.setFont(new Font("Tahoma", Font.BOLD, 16));
				
						scrollPane.setViewportView(tablFiguras);
						
						JLabel lblNewLabel_1 = new JLabel("M\u00C1XIMA SUPERFICIE");
						lblNewLabel_1.setFont(new Font("Bahnschrift", Font.BOLD, 20));
						lblNewLabel_1.setBounds(10, 96, 287, 29);
						frame.getContentPane().add(lblNewLabel_1);
						
						lblSuperficieMaxima = new JLabel("");
						lblSuperficieMaxima.setHorizontalAlignment(SwingConstants.CENTER);
						lblSuperficieMaxima.setBackground(Color.RED);
						lblSuperficieMaxima.setOpaque(true);
						lblSuperficieMaxima.setFont(new Font("Ubuntu", Font.BOLD, 20));
						lblSuperficieMaxima.setBounds(36, 128, 141, 25);
						frame.getContentPane().add(lblSuperficieMaxima);
		asignarValoresIniciales();
	}
	private void asignarValores(Cuadrado pCua){
		textNombre.setText(pCua.getNombre());
		textValor.setText(Float.toString(pCua.getLado()));
		
	}
	private void asignarValores(Circulo pCir){
		textNombre.setText(pCir.getNombre());
		textValor.setText(Float.toString(pCir.getRadio()));
	}
	private void asignarValores(Rectangulo pRec){
		textNombre.setText(pRec.getNombre());
		textBase.setText(Float.toString(pRec.getBase()));
		textAltura.setText(Float.toString(pRec.getAltura()));
	}
	private void asignarValores(TrianguloRectangulo pTri){
		textNombre.setText(pTri.getNombre());
		textBase.setText(Float.toString(pTri.getBase()));
		textAltura.setText(Float.toString(pTri.getAltura()));
	}
	
	private void llenarGrilla(List<Figura> pFiguras){
		int fila =0 ;
		arrayfiguras = new String[pFiguras.size()][4];
		DecimalFormat df = new DecimalFormat("#.##");
		for (Figura figura : pFiguras) {
			for(int col=0;col<4;col++){
				switch (col) {
				case 0:
					arrayfiguras[fila][col] = figura.getNombre();					
					break;
				case 1:
					arrayfiguras[fila][col] = figura.getValores();					
					break;					
				case 2:
					arrayfiguras[fila][col] = df.format(figura.calcularPerimetro());					
					break;
				case 3:
					arrayfiguras[fila][col] = df.format(figura.calcularSuperficie());					
					break;

				default:
					break;
				}				
			}
			fila++;
		}
		lblSuperficieMaxima.setText(df.format(Figura.getMaximaSuperficie()));
		tablFiguras.setModel(new DefaultTableModel(
				arrayfiguras,
				new String[] {
					"Nombre","Valores", "Perimetro", "Superficie"
				}
			));
	
	}
	private void asignarValoresIniciales(){
		figuras= new ArrayList<Figura>();
		figuras.add(new Cuadrado("Cubo 2D", 30));
		figuras.add(new Circulo("Circulito", 50));
		figuras.add(new Cuadrado("Cubo 3D", 40));
		figuras.add(new Circulo("Circulito grande", 60));
		
		llenarGrilla(figuras);		
	}
	private void limpiarCampos() {
		textNombre.setText("");
		textValor.setText("");
		textBase.setText("");
		textAltura.setText("");
	}
}